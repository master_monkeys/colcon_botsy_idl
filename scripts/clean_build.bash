#!/bin/bash
#
# Setup environment to make PX4 visible to Gazebo.


# clears build files
rm -r build/ && rm -r install/ && rm -r log/

# displays folder before build
ls

# builds ws
colcon build --parallel-workers $(nproc) --symlink-install
